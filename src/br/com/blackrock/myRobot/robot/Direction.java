package br.com.blackrock.myRobot.robot;

public enum Direction {
	
	RIGHT,
	LEFT,
	UP,
	DOWN;

}
