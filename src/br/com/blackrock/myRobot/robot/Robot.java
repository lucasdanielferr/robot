package br.com.blackrock.myRobot.robot;

import br.com.blackrock.myRobot.space.Space;

public class Robot implements Walk {
	
	private static final Integer FIRST_INDEX = 0;
	
	private Direction direction;
	private int currentX;
	private int currentY;
	private Space space;
	
	public Robot() {

		setDirection(Direction.RIGHT);
		setCurrentY(FIRST_INDEX);
		setCurrentX(FIRST_INDEX);
		
	}
	
	public int getCurrentX() {
		return currentX;
	}

	public void setCurrentX(int currentX) {
		this.currentX = currentX;
	}

	public int getCurrentY() {
		return currentY;
	}

	public void setCurrentY(int currentY) {
		this.currentY = currentY;
	}

	public Direction getDirection() {
		return direction;
	}
	
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	public Space getSpace() {
		return space;
	}
	
	public void setSpace(Space space) {
		this.space = space;
	}

	public void move(int steps) {	
		moveToDirection (getDirection(), steps);
	}
	
	@Override
	public void moveToDirection (Direction direction, int steps) throws IllegalStateException {
		
		if (this.space != null) {
			
			switch (direction) {
			case RIGHT:
				moveX(steps);
				break;
			case LEFT:
				moveX(-steps);
				break;
			case UP:
				moveY(-steps);
				break;
			case DOWN:
				moveY(steps);
				break;
			default:
				break;
			}
			
		} else {
			
			throw new IllegalStateException("There's no space to walk");
			
		}
		
	}
	
	private void moveX(int steps) {
		
		int x = getCurrentX() + steps;
		setCurrentX(validateIndex(x, space.getSizeX()));
		
	}
	
	private void moveY(int steps) {
		
		int y = getCurrentY() + steps;
		setCurrentY(validateIndex(y, space.getSizeY()));
		
	}
	
	private int validateIndex(int index, int size) {
		
		// Has the robot reached the end?
		if (index >= size)
			return index - size;
		if (index < FIRST_INDEX)
			return index + size;
		
		return index;
		
	}

}
