package br.com.blackrock.myRobot.robot;

public interface Walk {
	
	public void moveToDirection (Direction direction, int steps);

}
