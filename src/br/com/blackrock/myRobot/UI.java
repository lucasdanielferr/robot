package br.com.blackrock.myRobot;

import java.util.Scanner;

import br.com.blackrock.myRobot.robot.Direction;
import br.com.blackrock.myRobot.robot.Robot;

public class UI {
	
	private Scanner scanner;
	private Robot robot;
	
	public UI (Robot robot) {
		
		this.robot = robot;
		scanner = new Scanner(System.in);
		printMainMenu();
		
	}
	
	public void printMainMenu() {
		
		int option = 0;
		
		while (option != 4) {
			
			System.out.println("\n=========================================================");
			
			printSpace(robot.getCurrentY(), robot.getCurrentX());
			
			System.out.println("*** Rob� BlackRock ***");
			System.out.println("1 - Andar x casas");
			System.out.println("2 - Virar para x (Direita, Esquerda, Cima, Baixo)");
			System.out.println("3 - Virar para x e andar x casas");
			System.out.println("4 - Sair");
			
			option = scanner.nextInt();
			
			switch (option) {
			case 1:
				printWalkMenu();
				break;
			case 2:
				printDirectionMenu();
				break;
			case 3:
				printDirectionMenu();
				printWalkMenu();
				break;
			default:
				break;
			}
			
		}
		
	}
	
	public void printWalkMenu() {
		
		System.out.println("Andar quantas casas?");
		robot.move(scanner.nextInt());
		
	}
	
	public void printDirectionMenu() {
		
		System.out.println("Virar para?");
		System.out.println("1 - Direita");
		System.out.println("2 - Esquerda");
		System.out.println("3 - Cima");
		System.out.println("4 - Baixo");
		
		int direction = scanner.nextInt();
		
		switch (direction) {
		case 1:
			robot.setDirection(Direction.RIGHT);
			break;
		case 2:
			robot.setDirection(Direction.LEFT);
			break;
		case 3:
			robot.setDirection(Direction.UP);
			break;
		case 4:
			robot.setDirection(Direction.DOWN);
			break;
		default:
			break;
		}

	}
	
	public void printSpace (int curY, int curX) {
			
		for (int y = 0; y < robot.getSpace().getSizeY(); y++) {
			
			for (int x = 0; x < robot.getSpace().getSizeX(); x++) {
				
				if (curY == y && curX == x)
					System.out.print("R ");
				else
					System.out.print("- ");
				
			}
			
			System.out.print("\n");
			
		}

	}

}
