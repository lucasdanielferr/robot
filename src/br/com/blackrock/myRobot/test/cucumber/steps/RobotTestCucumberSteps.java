package br.com.blackrock.myRobot.test.cucumber.steps;

import br.com.blackrock.myRobot.robot.Robot;
import br.com.blackrock.myRobot.space.Space;
import br.com.blackrock.myRobot.space.SpaceToWalk;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertEquals;

public class RobotTestCucumberSteps {
	
	private Robot robot;
	
    @Given("^a robot turned to RIGHT at column (\\d+)$")
    public void a_robot_turned_to_RIGHT_at_column(int column) throws Throwable {

    	Space space = new SpaceToWalk();
    	robot = new Robot();
    	robot.setSpace(space);
    	robot.setCurrentX(column);;

    }

    @When("^we ask it to walk (\\d+) steps$")
    public void we_ask_it_to_walk_steps(int steps) throws Throwable {
    	
    	robot.move(steps);
    	
    }

    @Then("^the robot must go to column (\\d+)$")
    public void the_robot_must_go_to_column(int expected_x) throws Throwable {
    	assertEquals(expected_x, robot.getCurrentX());
    }

}
