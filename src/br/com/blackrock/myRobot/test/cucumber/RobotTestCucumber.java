package br.com.blackrock.myRobot.test.cucumber;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		tags = "@RobotTestCucumber", 
		glue = "steps", 
		monochrome = true, 
		dryRun = false)
public class RobotTestCucumber {


}
