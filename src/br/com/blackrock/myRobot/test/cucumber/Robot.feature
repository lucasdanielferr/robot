# language: en
  @RobotTestCucumber
  Feature: Test Robot Behavior
    The robot must be able to walk through a given space (size 10x10). 
    Also, it should respond to the following commands: 1) walk N steps; 2) turn to a certain direction (right, left, up, down).
    Finally, it's a circular space, so, once the robot reach the end of space, it turns back to beginning.
   
    Scenario Outline: Test sequential movements, crossing the end of space 
      Given a robot turned to RIGHT at column <x>
      When we ask it to walk <steps> steps
      Then the robot must go to column <expected_x>
   
      Examples: 
        | x | steps | expected_x |
        | 0 | 3     | 3          |
        | 3 | 1     | 4          |
        | 4 | 3     | 7          |
        | 7 | 2     | 9          |
        | 9 | 5     | 4          |