package br.com.blackrock.myRobot.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.blackrock.myRobot.robot.Direction;
import br.com.blackrock.myRobot.robot.Robot;
import br.com.blackrock.myRobot.space.Space;

public class RobotTest {
	
	@Mock
	Space space = null;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testMockito() {
	
		Mockito.when(space.getSizeX()).thenReturn(10);
		Mockito.when(space.getSizeY()).thenReturn(10);

		// running
		Robot robot = new Robot();
		robot.setSpace(space);
		robot.moveToDirection(Direction.RIGHT, 5);
		
		// verifying
		assertEquals(5, robot.getCurrentX());
		
		Mockito.verify(space, Mockito.times(1)).getSizeX();
		Mockito.verify(space, Mockito.never()).getSizeY();
		
	}
	
	@Test
	public void testMoveToRight() {
		
		Mockito.when(space.getSizeX()).thenReturn(10);
		Mockito.when(space.getSizeY()).thenReturn(10);
		
		// running
		Robot robot = new Robot();
		robot.setSpace(space);
		robot.moveToDirection(Direction.RIGHT, 6);
		
		// verifying
		assertEquals(6, robot.getCurrentX());
		
	}
	
	@Test
	public void testSequentialMovements() {
		
		Mockito.when(space.getSizeX()).thenReturn(10);
		Mockito.when(space.getSizeY()).thenReturn(10);
		
		// running
		Robot robot = new Robot();
		robot.setSpace(space);
		
		// movement: 1
		robot.moveToDirection(Direction.RIGHT, 7);
		assertEquals(0, robot.getCurrentY());
		assertEquals(7, robot.getCurrentX());
		
		// movement: 2
		robot.moveToDirection(Direction.DOWN, 5);
		assertEquals(5, robot.getCurrentY());
		assertEquals(7, robot.getCurrentX());
		
		// movement: 3
		robot.moveToDirection(Direction.LEFT, 3);
		assertEquals(5, robot.getCurrentY());
		assertEquals(4, robot.getCurrentX());
		
		// movement: 4
		robot.moveToDirection(Direction.UP, 2);
		assertEquals(3, robot.getCurrentY());
		assertEquals(4, robot.getCurrentX());
		
	}
	
	@Test
	public void testCrossingBorder() {
		
		Mockito.when(space.getSizeX()).thenReturn(10);
		Mockito.when(space.getSizeY()).thenReturn(10);
		
		// running
		Robot robot = new Robot();
		robot.setSpace(space);
		
		// movement: 1
		robot.moveToDirection(Direction.RIGHT, 5);
		assertEquals(0, robot.getCurrentY());
		assertEquals(5, robot.getCurrentX());
		
		// movement: 2
		robot.moveToDirection(Direction.DOWN, 5);
		assertEquals(5, robot.getCurrentY());
		assertEquals(5, robot.getCurrentX());
		
		// movement: 3
		robot.moveToDirection(Direction.RIGHT, 8);
		assertEquals(5, robot.getCurrentY());
		assertEquals(3, robot.getCurrentX());
		
	}

}
