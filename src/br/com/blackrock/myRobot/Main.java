package br.com.blackrock.myRobot;

import br.com.blackrock.myRobot.robot.Robot;
import br.com.blackrock.myRobot.space.Space;
import br.com.blackrock.myRobot.space.SpaceToWalk;

public class Main {

	public static void main(String[] args) {
		
		Space space = new SpaceToWalk();
		
		Robot robot = new Robot();
		robot.setSpace(space);
		
		new UI(robot);
	}

}
