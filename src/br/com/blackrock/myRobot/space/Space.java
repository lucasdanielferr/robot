package br.com.blackrock.myRobot.space;

public interface Space {
	
	public int getSizeX();
	public int getSizeY();

}
