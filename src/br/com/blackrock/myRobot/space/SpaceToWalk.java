package br.com.blackrock.myRobot.space;

public class SpaceToWalk implements Space {

	private static final int sizeX = 10;
	private static final int sizeY = 10;
	
	@Override
	public int getSizeX() {
		return sizeX;
	}

	@Override
	public int getSizeY() {
		return sizeY;
	}
	
}
